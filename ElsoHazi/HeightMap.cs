﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ElsoHazi
{

    class HeightMap
    {
        short[,] heightDatas;
        const int SIZEX = 1201;
        const int SIZEY = 1201;

        public int ElevationThreshold
        {
            get;
            set;
        }

        public void Parse(string path)
        {
            byte[] twoByteValue = new byte[2];
            heightDatas = new short[SIZEX, SIZEY];
            FileStream fs;
            using (fs = new FileStream(path, FileMode.Open))
            {
                int bytesRead;
                int rowCounter = 0;
                int columnCounter = 0;
                while ((bytesRead = fs.Read(twoByteValue, 0, 2)) != 0)
                {

                    Array.Reverse(twoByteValue);
                    short littleEndianNumber = BitConverter.ToInt16(twoByteValue, 0);
                    heightDatas[rowCounter, columnCounter] = littleEndianNumber;
                    rowCounter = rowCounter + 1;
                    if (rowCounter == 1201)
                    {
                        columnCounter = columnCounter + 1;
                        rowCounter = 0;
                    }
                }
            }
        }

        private short[] GetExtremalElevations(short[,] array)
        {
            short maxValue = array[0,0];
            short minValue = array[0,0];
            short[] result=new short[2];
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    if (array[i, y] > maxValue)
                    {
                        maxValue = array[i, y];
                    }
                    if (array[i, y]<minValue)
                    {
                        minValue = array[i, y];
                    }
                }
            }
            result[0] = minValue;
            result[1] = maxValue;
            return result;
        }

        public void SaveToBitmap(string path)
        {
            short[] extremalElevations = GetExtremalElevations(heightDatas);
            short min = extremalElevations[0];
            short max = extremalElevations[1];
            Bitmap bmp = new Bitmap(SIZEX, SIZEY);

            short oldValueRange = (short)(max - ElevationThreshold);
            for (int x = 0; x < heightDatas.GetLength(0); x++)
            {
                for (int y = 0; y < heightDatas.GetLength(1); y++)
                {
                    if (heightDatas[x, y] < ElevationThreshold)
                    {
                        Color c = Color.FromArgb(0, 0, 255);
                        bmp.SetPixel(x, y, c);
                    }
                    else if (heightDatas[x, y] == 32768)
                    {
                        Color c = Color.FromArgb(255, 0, 0);
                        bmp.SetPixel(x, y, c);
                    }
                    else
                    {
                        int green = (((heightDatas[x, y] - ElevationThreshold) * 255) / oldValueRange);
                        int green2 = 255 - green;
                        Color c = Color.FromArgb(green2, 255, green2);
                        bmp.SetPixel(x, y, c);
                    }
                }
            }
            bmp.Save(path);
        }
    }
}
